﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SpaceBox
{
    public partial class Form1 : Form
    {
        //lamar a la clase nave
        Nave nav;
        // llamar a la clase movimientos
        Movimientos mov = new Movimientos();
        //creación de la matriz para el tablero
        string [,] tablero = new string[15,15];
        //variable de la ubicación
        string ubicacion = null;
        //variable de la carpeta
        string carpetaBusqueda = "";
        //variable para los puntos
        int puntos = 0;
        //variable para el nombre del piloto
        string nombre = null;
        //variable para el apellido del piloto
        string apellido = null;
        //variable para el contador de los movimientos
        int cont = 0;
        //variable pra el recorrido
        int cont2=0;
        //posición de las filas
        int pos1=0;
        //Variable para la posición de las columnas
        int pos2 = 0;
        public Form1()
        {
            InitializeComponent();
            
            //volver invisible el label
            label3.Visible = false;
            //volver invisible el label
            label4.Visible = false;
            //volver invisible el textbox
            txtNombre.Visible = false;
            //volver invisible el textbox
            txtApellido.Visible = false;
            //volver invisible el botón
            btnSimulación.Visible = false;
        }


        private void btnInstrucciones_Click(object sender, EventArgs e)
        {
            //TODO
        }

        private void btnSalir1_Click(object sender, EventArgs e)
        {
            //cerrar el programa
            this.Close();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
        //TODO
        }

        private void btnArchivo_Click(object sender, EventArgs e)
        {
            //variable para adjuntar los errores
            string errores = "";
            //si la carpeta es igual a vacío entonces tirará error
            if (carpetaBusqueda == "")
            {
                //mensaje de error
                MessageBox.Show("Debe seleccionar una carpeta");
            }
                //en caso contrario hará esto
            else
            {
                //Archivos
                for (int i = 0; i < Directory.EnumerateFiles(carpetaBusqueda).Count(); i++)
                {
                    //variable para guardar el nombre de la ubicación
                    FileInfo k = new FileInfo(Directory.GetFiles(carpetaBusqueda)[i]);//
                    //si el nombre es igual al del textbos entonces hara lo siguiente
                    if (k.Name == txtArchivo.Text)
                    {
                        //mensaje de que fue encontrado
                        MessageBox.Show("Archivo encontrado en " + k.FullName);
                        //variable para guardar la ubicación
                        ubicacion = k.FullName;
                    }


                }
                //llamar a la clase de lectura
                lecturaArchivo lectura = new lecturaArchivo();
                //variable para poder validar según el método
                errores = lectura.validar(@ubicacion);
                //si existen errores entonces hará lo siguiente
                if (errores != "")
                {
                    //tirará un mensaje de error
                    lbErrores.Text = errores + "Ingrese de nuevo";
                    //vuelve invisible al label
                    label3.Visible = false;
                    //vuelve invisible al label
                    label4.Visible = false;
                    //vuelve invisible al textbox
                    txtNombre.Visible = false;
                    //vuelve invisible al textbox
                    txtApellido.Visible = false;
                    //vuelve invisible el botón
                    btnSimulación.Visible = false; 
                }
                    //en caso contrario hará lo siguiente
                else
                {
                    //llama al metodo de generar el tablero
                    generarTablero();
                    //deja en blanco el label
                    lbErrores.Text = "";
                    //vuelve visible el label
                    label3.Visible = true;
                    //vuelve visible el label
                    label4.Visible = true;
                    //vuelve visible el textbox
                    txtNombre.Visible = true;
                    //vuelve visible el textbox
                    txtApellido.Visible = true;
                    //vuelve visible el botón
                    btnSimulación.Visible = true;
                }
            }
                
           
        }

        private void btnCarpeta_Click(object sender, EventArgs e)
        {
            //Crear cuadro de seleccionar carpeta
            FolderBrowserDialog Carpeta = new FolderBrowserDialog();
            //si el usuario pulsa ok hará lo siguiente
            if (Carpeta.ShowDialog() == DialogResult.OK)
            {
                //carpeta que seleccionamos
                carpetaBusqueda = Carpeta.SelectedPath;
            }
        }

        private void btnSimulación_Click(object sender, EventArgs e)
        {
           
                //llamar a la clase de la nave
                nav = new Nave();
            //asignar variable al texto de nombre
                nombre = txtNombre.Text;
                //asignar variable al texto de apellido
                apellido = txtApellido.Text;
                //asignar variable para volverlo a mayusculas
                string ini = nombre.ToUpper();
                //asignar variable para volverlo a mayusculas
                string fin = apellido.ToUpper();
            //obtener la inicial mediante el método
                string inicial = nav.obtenerInicialNombre(ini);
            //obtener la última letra mediante el método
                string final = nav.obtenerFinalApellido(fin);
            //obtener el largo según el método
                int largo = nav.largo(nombre, apellido);
            //imprimir en el label el nombre de la nave
                lbNom.Text = "GUA-" + inicial + final + "-" + largo;
                tabControl1.SelectedIndex = 2; //tab 3
            //dejar en blanco el textbox
                txtArchivo.Text = "";
                //dejar en blanco el textbox
                txtNombre.Text = "";
                //dejar en blanco el textbox
                txtApellido.Text = "";
            //volver invisible el label
                label3.Visible = false;
                //volver invisible el label
                label4.Visible = false;
                //volver invisible el textbox
                txtNombre.Visible = false;
                //volver invisible el textbox
                txtApellido.Visible = false;
                //volver invisible el botón
                btnSimulación.Visible = false;

            
            
        }
        //asignar variable para leer las líneas
        string lin;
        public void generarTablero() //método para generar el tablero
        {
            //instancia para poder leer el texto
            System.IO.StreamReader sr = new System.IO.StreamReader(@ubicacion); 
            //ciclo para poder recorrer las filas
            for (int i = 0; i < 15; i++)
            {
                //agregar filas al data
                dataGridView1.Rows.Add();
                //instancia para poder acomodar las imágenes de acuerdo a la celda
                ((DataGridViewImageColumn)dataGridView1.Columns[i]).ImageLayout = DataGridViewImageCellLayout.Stretch;
                //para poder leer el texto
                lin = sr.ReadLine();
                //variable var para poder convertir las lineas en un vector y así poder leerlas
                var filas = lin.ToCharArray();
                //ciclo for para poder recorrer las columnas
                for (int j = 0; j < 15; j++)
                {
                    //convertir la posción de la matriz en string para poder así compararla luego
                    tablero[i, j] = Convert.ToString(filas[j]); 
                    //si la posición es igual a A entonces hara lo siguiente
                    if (tablero[i, j] == "A")
                    {
                        //Bitmap image = new Bitmap("Blanco.jpeg");
                        //imprimir la imagen del color gris según la posición
                        dataGridView1.Rows[i].Cells[j].Value = Properties.Resources.BlancoUno;
                    }
                        //en caso sea igual a B hara lo siguiente
                    else if (tablero[i, j] == "B")
                    {
                        //Bitmap image2 = new Bitmap("SpaceBox.jpeg");
                        //imprimir la imagen del SpaceBox gris según la posición
                        dataGridView1.Rows[i].Cells[j].Value = Properties.Resources.SpaceBoxUno ;
                        //la posicion 1 se convierte en i
                        pos1 = i;
                        //la posicion 2 se convierte en j
                        pos2 = j;
                    }
                    //en caso sea igual a C hara lo siguiente
                    else if (tablero[i, j] == "C")
                    {
                        //Bitmap image3 = new Bitmap("Asteroide.jpeg"); 
                        //imprimir la imagen del asteroide según la posición
                        dataGridView1.Rows[i].Cells[j].Value = Properties.Resources.AsteroideUno ;
                    }
                    //en caso sea igual a D hara lo siguiente
                    else if (tablero[i, j] == "D")
                    {
                       // Bitmap image4 = new Bitmap("Tierra.jpeg");
                        //imprimir la imagen de la Tierra gris según la posición
                        dataGridView1.Rows[i].Cells[j].Value = Properties.Resources.TierraUno ;
                    }
                    //en caso sea igual a E hara lo siguiente
                    else if (tablero[i, j] == "E")
                    {
                        //Bitmap image5 = new Bitmap("Cristal.jpeg");
                        //imprimir la imagen del cristal gris según la posición
                        dataGridView1.Rows[i].Cells[j].Value = Properties.Resources.CristalUno;
                    }
                }
            }
        }

        private void btnSalirTablero_Click(object sender, EventArgs e)
        {
            //Si el usuario pulsa Yes entonces hará lo siguiente
            if (DialogResult.Yes == MessageBox.Show("Desea salir?", "SpaceBox", MessageBoxButtons.YesNo))
            {
                //CERRAR EL PROGRAMA
                this.Close();
            }
        
        }

        private void btnReinicio_Click(object sender, EventArgs e)
        {
            //si el usuario pulsa Yes entonces hará lo siguiente
            if (DialogResult.Yes == MessageBox.Show("Desea reiniciar?", "SpaceBox", MessageBoxButtons.YesNo))
            {
                //deja el label en blanco
                lbEsp.Text = "";
                //deja el label en blanco
                lbMov.Text = "";
                //deja el label en blanco
                lbPun.Text = "";
                //vuelve a iniciar el contador en 0
                cont = 0;
                //vuelve a iniciar el contador en 0
                cont2 = 0;
                //llama al metodo para poder volver a generar el tablero
                generarTablero();
            }
        }
        protected override bool ProcessCmdKey(ref Message res, Keys teclatl ) //
        {
            //inicialización de variables para los posiciones
              int x,y;
            //si la tecla que pulsa es igual hacia arriba hará lo siguiente
            if (teclatl == Keys.Up) 
            {
                //Agrega un valor al contador
                cont++; //
                //convierte el valor en sitrng para imprimirlo
                lbMov.Text = Convert.ToString(cont);
                //x se convierte en la poscicion de las filas
                x = pos1;
                //y se convierte en la poscicion de las columnas
                y = pos2; 
                //imprime la imagen del color gris según la posición
                dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.GrisUno;// 
                //la posición sera igual a A
                tablero[x, y] = "A";
                //se utiliza el metodo para mover hacia la izquierda
                int fila = mov.Arriba(ref x, ref y, tablero); 
                //si la fila es mayor a -1 entonces hará lo siguiente
                if (fila > -1){
                    //ciclo for para saber como se mueve la nave
                    for (int i = pos1; i > x; i--)
                    {
                        //Agrega valor al contador de los espacios recorridos
                        cont2++; 
                        //imprime el valor en el label
                        lbEsp.Text = Convert.ToString(cont2);
                        //imprime la imagen del color gris según se haya movido
                        dataGridView1.Rows[i].Cells[y].Value = Properties.Resources.GrisUno;//anteess pdee espera jaja, hay un error acá
                    }
                    //si la posción de la matriz es igual a D entonces hará lo siguiente
                    if (tablero[x, y] == "D") 
                    {
                        //imprimirá la imagen del fin de la simulación
                        dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.Fin;
                        //imprime el mensaje correcto y si el usuario pulsa Yes entonces hara lo siguiente
                        if (DialogResult.Yes == MessageBox.Show("¡¡¡Ha encontrado un camino correcto!!!" + "\n" + "Si desea volver al menú pulse Sí", "SpaceBox", MessageBoxButtons.YesNo))
                        {
                            //regresar al tab número 1
                            tabControl1.SelectedIndex = 0; 
                        }
                        {
                            
                        }
                    }
                    //y se convierte la posición de la nave nuevamente
                    tablero[x, y] = "B";
                    //la posición 1 agarra el valor de x
                    pos1 = x;
                    //la posición 2 agarra el valor de y
                    pos2 = y;
                    //imprime la imagen del SpaceBox según la posición
                    dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.SpaceBoxUno;
                }
                    //en caso contrario hará lo siguiente
                else 
                {
                    //ciclo for para saber como se mueve la nave
                    for (int i = pos1; i >= x; i--)
                    {
                        //Agrega valor al contador de los espacios recorridos
                        cont2++;
                        //imprime el valor en el label
                        lbEsp.Text = Convert.ToString(cont2);
                        //imprime la imagen del color gris según se haya movido
                        dataGridView1.Rows[i].Cells[y].Value = Properties.Resources.GrisUno; 
                    }
                    //y se convierte la posición de la nave nuevamente
                    tablero[x, y] = "B";
                    //la posición 1 agarra el valor de x
                    pos1 = x;
                    //la posición 2 agarra el valor de y
                    pos2 = y;
                    //imprime la imagen del SpaceBox según la posición
                    dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.SpaceBoxUno; 
                    //generar el mensaje de error
                    if (DialogResult.Yes == MessageBox.Show("¡¡¡Se encuentra en espacio no explorado!!!" + "\n" + "Si desea reiniciar, pulse Sí", "SpaceBox", MessageBoxButtons.YesNo))
                    {
                        //generar nuevamente el tablero si pulsa Yes
                        generarTablero();
                    }
                
                }
                //retornar el valor de verdadero
                return true;
            }
            //si la tecla que pulsa es igual hacia la izquierda hará lo siguiente
            else if (teclatl == Keys.Left)
            {
                //Agrega un valor al contador
                cont++; //
                //convierte el valor en sitrng para imprimirlo
                lbMov.Text = Convert.ToString(cont);
                //x se convierte en la poscicion de las filas
                x = pos1;
                //y se convierte en la poscicion de las columnas
                y = pos2;
                //imprime la imagen del color gris según la posición
                dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.GrisUno;
                //la posición sera igual a A
                tablero[x, y] = "A";
                //se utiliza el metodo para mover hacia la izquierda
                int columna = mov.Izquierda(ref x, ref y, tablero); 
                //si la columna es mayor a .1 entonces hará lo siguiente
                if (columna > -1)
                {
                    //ciclo for para saber como se mueve la nave
                    for (int j = pos2; j > y; j--)
                    {
                        //Agrega valor al contador de los espacios recorridos
                        cont2++;
                        //imprime el valor en el label
                        lbEsp.Text = Convert.ToString(cont2);
                        //imprime la imagen del color gris según se haya movido
                        dataGridView1.Rows[x].Cells[j].Value = Properties.Resources.GrisUno;
                    }

                    if (tablero[x, y] == "D") // si termina en una posicion "D", gana
                    {
                        //imprimirá la imagen del fin de la simulación
                        dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.Fin;
                        //imprime el mensaje correcto y si el usuario pulsa Yes entonces hara lo siguiente
                        if (DialogResult.Yes == MessageBox.Show("¡¡¡Ha encontrado un camino correcto!!!" + "\n" + "Si desea volver al menú pulse Sí", "SpaceBox", MessageBoxButtons.YesNo))
                        {
                            //regresar al tab número 1
                            tabControl1.SelectedIndex = 0;
                        }
                        {

                        }
                    }
                    //y se convierte la posición de la nave nuevamente
                    tablero[x, y] = "B";
                    //la posición 1 agarra el valor de x
                    pos1 = x;
                    //la posición 2 agarra el valor de y
                    pos2 = y;
                    //imprime la imagen del SpaceBox según la posición
                    dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.SpaceBoxUno;
                }
                else
                {
                    for (int j = pos1; j >= y; j--)
                    {
                        //Agrega valor al contador de los espacios recorridos
                        cont2++;
                        //imprime el valor en el label
                        lbEsp.Text = Convert.ToString(cont2);
                        //imprime la imagen del color gris según se haya movido
                        dataGridView1.Rows[x].Cells[j].Value = Properties.Resources.GrisUno; //te gusta mi forma de que la gente no me pregunte mas jaja
                    }
                    //y se convierte la posición de la nave nuevamente
                    tablero[x, y] = "B";
                    //la posición 1 agarra el valor de x
                    pos1 = x;
                    //la posición 2 agarra el valor de y
                    pos2 = y;
                    //imprime la imagen del SpaceBox según la posición
                    dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.SpaceBoxUno;
                    //generar el mensaje de error
                    if (DialogResult.Yes == MessageBox.Show("¡¡¡Se encuentra en espacio no explorado!!!" + "\n" + "Si desea reiniciar, pulse Sí", "SpaceBox", MessageBoxButtons.YesNo))
                    {
                        //generar nuevamente el tablero si pulsa Yes
                        generarTablero();
                    }

                }
                //retornar el valor de verdadero
                return true;
            }
            //si la tecla que pulsa es igual hacia abajo hará lo siguiente
            else if (teclatl == Keys.Down)
            {
                //Agrega un valor al contador
                cont++; //
                //convierte el valor en sitrng para imprimirlo
                lbMov.Text = Convert.ToString(cont);
                //x se convierte en la poscicion de las filas
                x = pos1;
                //y se convierte en la poscicion de las columnas
                y = pos2;
                //imprime la imagen del color gris según la posición
                dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.GrisUno;// 
                //la posición sera igual a A
                tablero[x, y] = "A";
                //se utiliza el metodo para mover hacia la izquierda
                int fila = mov.Abajo(ref x, ref y, tablero); 
                //si la fila es menor a 15 hará lo siguiente
                if (fila < 15)
                {
                    //ciclo for para saber como se mueve la nave
                    for (int i = pos1; i < x; i++)
                    {
                        //Agrega valor al contador de los espacios recorridos
                        cont2++;
                        //imprime el valor en el label
                        lbEsp.Text = Convert.ToString(cont2);
                        //imprime la imagen del color gris según se haya movido
                        dataGridView1.Rows[i].Cells[y].Value = Properties.Resources.GrisUno;//anteess pdee espera jaja, hay un error acá
                    }

                    if (tablero[x, y] == "D") // si termina en una posicion "D", gana
                    {
                        //imprimirá la imagen del fin de la simulación
                        dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.Fin;
                        //imprime el mensaje correcto y si el usuario pulsa Yes entonces hara lo siguiente
                        if (DialogResult.Yes == MessageBox.Show("¡¡¡Ha encontrado un camino correcto!!!" + "\n" + "Si desea volver al menú pulse Sí", "SpaceBox", MessageBoxButtons.YesNo))
                        {
                            //regresar al tab número 1
                            tabControl1.SelectedIndex = 0;
                        }
                        {

                        }
                    }
                    //y se convierte la posición de la nave nuevamente
                    tablero[x, y] = "B";
                    //la posición 1 agarra el valor de x
                    pos1 = x;
                    //la posición 2 agarra el valor de y
                    pos2 = y;
                    //imprime la imagen del SpaceBox según la posición
                    dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.SpaceBoxUno;
                }
                else
                {
                    for (int i = pos1; i <= x; i++)
                    {
                        //Agrega valor al contador de los espacios recorridos
                        cont2++;
                        //imprime el valor en el label
                        lbEsp.Text = Convert.ToString(cont2);
                        //imprime la imagen del color gris según se haya movido
                        dataGridView1.Rows[i].Cells[y].Value = Properties.Resources.GrisUno; //te gusta mi forma de que la gente no me pregunte mas jaja
                    }
                    //y se convierte la posición de la nave nuevamente
                    tablero[x, y] = "B";
                    //la posición 1 agarra el valor de x
                    pos1 = x;
                    //la posición 2 agarra el valor de y
                    pos2 = y;
                    //imprime la imagen del SpaceBox según la posición
                    dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.SpaceBoxUno;
                    //generar el mensaje de error
                    if (DialogResult.Yes == MessageBox.Show("¡¡¡Se encuentra en espacio no explorado!!!" + "\n" + "Si desea reiniciar, pulse Sí", "SpaceBox", MessageBoxButtons.YesNo))
                    {
                        //generar nuevamente el tablero si pulsa Yes
                        generarTablero();
                    }

                }
                //retornar el valor de verdadero
                return true;
            }
            //si la tecla que pulsa es igual hacia la derecha hará lo siguiente
            else if (teclatl == Keys.Right) 
            {
                //Agrega un valor al contador
                cont++; //
                //convierte el valor en sitrng para imprimirlo
                lbMov.Text = Convert.ToString(cont);
                //x se convierte en la poscicion de las filas
                x = pos1;
                //y se convierte en la poscicion de las columnas
                y = pos2;
                //imprime la imagen del color gris según la posición
                dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.GrisUno;// 
                //la posición sera igual a A
                tablero[x, y] = "A";
                //se utiliza el metodo para mover hacia la izquierda
                int columna = mov.Derecha(ref x, ref y, tablero); 
                //si la columna es menor a 15 entonces hará lo siguiente
                if (columna < 15)
                {
                    //ciclo for para saber como se mueve la nave
                    for (int j = pos2; j < y; j++)
                    {
                        //Agrega valor al contador de los espacios recorridos
                        cont2++;
                        //imprime el valor en el label
                        lbEsp.Text = Convert.ToString(cont2);
                        //imprime la imagen del color gris según se haya movido
                        dataGridView1.Rows[x].Cells[j].Value = Properties.Resources.GrisUno;
                    }

                    if (tablero[x, y] == "D") // si termina en una posicion "D", gana
                    {
                        //imprimirá la imagen del fin de la simulación
                        dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.Fin;
                        //imprime el mensaje correcto y si el usuario pulsa Yes entonces hara lo siguiente
                        if (DialogResult.Yes == MessageBox.Show("¡¡¡Ha encontrado un camino correcto!!!" + "\n" + "Si desea volver al menú pulse Sí", "SpaceBox", MessageBoxButtons.YesNo))
                        {
                            //regresar al tab número 1
                            tabControl1.SelectedIndex = 0;
                        }
                        {

                        }
                    }
                    //y se convierte la posición de la nave nuevamente
                    tablero[x, y] = "B";
                    //la posición 1 agarra el valor de x
                    pos1 = x;
                    //la posición 2 agarra el valor de y
                    pos2 = y;
                    //imprime la imagen del SpaceBox según la posición
                    dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.SpaceBoxUno;
                }
                    //en caso contrario hará lo siguiente
                else
                {
                    for (int j = pos1; j <= y; j++)
                    {
                        //Agrega valor al contador de los espacios recorridos
                        cont2++;
                        //imprime el valor en el label
                        lbEsp.Text = Convert.ToString(cont2);
                        //imprime la imagen del color gris según se haya movido
                        dataGridView1.Rows[x].Cells[j].Value = Properties.Resources.GrisUno; //aca deberia colorear los grises
                    }
                    //y se convierte la posición de la nave nuevamente
                    tablero[x, y] = "B";
                    //la posición 1 agarra el valor de x
                    pos1 = x;
                    //la posición 2 agarra el valor de y
                    pos2 = y;
                    //imprime la imagen del SpaceBox según la posición
                    dataGridView1.Rows[x].Cells[y].Value = Properties.Resources.SpaceBoxUno;
                    //generar el mensaje de error
                    if (DialogResult.Yes == MessageBox.Show("¡¡¡Se encuentra en espacio no explorado!!!" + "\n" + "Si desea reiniciar, pulse Sí", "SpaceBox", MessageBoxButtons.YesNo))
                    {
                        //generar nuevamente el tablero si pulsa Yes
                        generarTablero();
                    }

                }
                //retornar el valor de verdadero
                return true;
            }

            //retornar el valor metodo en base a la referencia del resultado y de la tecla pulsada
            return base.ProcessCmdKey(ref res, teclatl);
        }
    }
}
//
