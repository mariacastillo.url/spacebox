﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceBox
{
    class Movimientos
    {
        public int Arriba(ref int i, ref int j, string[,] tablero)//método para el movimiento hacia arriba
        {
            //la variable fila agarra el valor de la posición en i 
            int fila = i; 
            //la variable con valor de verdaero
            bool caminoLibre= true; 
            //mientras el camino sea verdadero hará lo siguiente
            while (caminoLibre == true) 
            {
                //si la posición en i-1 es mayor a -1 hara lo siguiente
                if (i - 1 > -1)
                {
                    //si la posición del tablero es igual a A entonces...
                    if (tablero[i - 1, j] == "A") 
                    {
                        //restará un valor a la fila 
                        fila= fila -1;
                        //restará un valor a i
                        i= i-1; 
                    }
                        //En caso de que sea B no hará nada
                    else if (tablero[i - 1, j] == "B") 
                    {

                    }
                        //en caso de que sea C
                    else if (tablero[i - 1, j] == "C")
                    {
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                    else if (tablero[i - 1, j] == "D")
                    {
                        //se le restará un valor a la fila
                        fila= fila-1;
                        //se le restará un valor a i 
                        i= i-1;
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                    else if (tablero[i - 1, j] == "E") 
                    {
                        //se le restará un valor a la fila
                        fila = fila - 1;
                        //se le restará un valor a i 
                        i = i - 1;
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                }
                else
                {
                    fila = - 1;
                    caminoLibre = false;
                }
            }
            //retornar el valor de la fila
            return fila;
        }
        public int Izquierda(ref int i, ref int j, string[,] tablero) //método para el movimiento hacia la izquierda
        {
            //la variable columna agarra el valor de la posición en j
            int columna = j;
            //la variable con valor de verdaero
            bool caminoLibre = true;
            //mientras el camino sea verdadero hará lo siguiente
            while (caminoLibre == true) 
            {
                if (j - 1 > -1)
                {
                    // si es A entonces
                    if (tablero[i, j - 1] == "A")
                    {
                        columna = columna - 1;
                        j = j - 1;
                    }
                    //En caso de que sea B no hará nada
                    else if (tablero[i, j - 1] == "B")
                    {

                    }
                        //en caso de que sea C
                    else if (tablero[i, j - 1] == "C")
                    {
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                    else if (tablero[i, j - 1] == "D")
                    {
                        //se le restará un valor a la columna
                        columna = columna - 1;
                        //se le restará un valor a j
                        j = j - 1;
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                    else if (tablero[i, j - 1] == "E")
                    {
                        //se le restará un valor a la columna
                        columna = columna - 1;
                        //se le restará un valor a j
                        j = j - 1;
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                }
                else
                {
                    columna = -1;
                    caminoLibre = false;
                }
            }//retornar el valor de la columna
            return columna;
        }
        public int Abajo(ref int i, ref int j, string[,] tablero) //método para el movimiento hacia abajo
        {
            //la variable fila agarra el valor de la posición en i 
            int fila = i;
            //la variable con valor de verdaero
            bool caminoLibre = true;
            //mientras el camino sea verdadero hará lo siguiente
            while (caminoLibre == true) 
            {
                if (i + 1 < 15) //porque va aumentando
                {
                    // si es A entonces
                    if (tablero[i + 1, j] == "A")
                    {
                        fila = fila + 1;
                        i = i + 1;
                    }
                    //En caso de que sea B no hará nada
                    else if (tablero[i + 1, j] == "B")
                    {

                    }
                        //en caso de que sea C
                    else if (tablero[i + 1, j] == "C")
                    {
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                    else if (tablero[i + 1, j] == "D")
                    {
                        //se le agregara un valor a la fila
                        fila = fila + 1;
                        //se le agregara un valor a i
                        i = i + 1;
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                    else if (tablero[i + 1, j] == "E")
                    {
                        //se le agregara un valor a la fila
                        fila = fila + 1;
                        //se le agregara un valor a i
                        i = i + 1;
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                }
                else
                {
                    fila = 15;
                    caminoLibre = false;//solo eso, toda esta bien? falta una cosa
                }
            }
            //retornar el valor de la fila
            return fila;
        }
        public int Derecha(ref int i, ref int j, string[,] tablero) //método para el movimiento haciala derecha
        {
            //la variable columna agarra el valor de la posición en j
            int columna = j;
            //la variable con valor de verdaero
            bool caminoLibre = true;
            //mientras el camino sea verdadero hará lo siguiente 
            while (caminoLibre == true)
            {
                if (j + 1 < 15)
                {
                    // si es A entonces
                    if (tablero[i, j+1] == "A")
                    {
                        columna = columna - 1;
                        j = j + 1;
                    }
                    //En caso de que sea B no hará nada
                    else if (tablero[i, j+1] == "B")
                    {

                    }
                        //en caso de que sea C
                    else if (tablero[i , j+1] == "C")
                    {
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                        // en casode que sea D
                    else if (tablero[i, j+1] == "D")
                    {
                        //se le agregará un valor a la columna
                        columna = columna + 1;
                        //Se le agregará un a valor a j
                        j = j + 1;
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                        //en caso de que sea E
                    else if (tablero[i , j+1] == "E")
                    {
                        //se le agregará un valor a la columna
                        columna = columna + 1;
                        //Se le agregará un a valor a j
                        j = j + 1;
                        //se cambiara el valor a falso
                        caminoLibre = false;
                    }
                }
                else
                {
                    columna = 15;
                    caminoLibre = false;
                }
            }
            //retornar el valor de la columna 
            return columna;
        }
        
    }
}
