﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpaceBox
{
    class lecturaArchivo
    {

        public string validar(string ubicacion) //método para validar el texto
        {
            //respuesta a enviar 
            string error = "";
            //variable para leer las líneas
            string l = "";
            //variable bool para saber el largo de las líneas
            bool largo = true;
            //variable char para detectar en el texto
            char letra;
            //variable para saber cuantas naves hay
            int spaceBox = 0;
            //variable para saber cuantas tierras hay
            int tierra = 0;
            //variable para saber cuantos cristales hay
            int crist = 0;
            //variable para saber cuantos espacios en blanco hay
            int espaciosBlancos = 0;
            //variable para saber cuantos asteroides hay
            int asteroides = 0;
            //variable para saber cuantos caracteres no identificados hay
            int noIdentificados = 0;
            int cont = 0;//contador de las líneas

            //instancia para poder leer el archivo según su ubicación
            System.IO.StreamReader sr=  new System.IO.StreamReader(@ubicacion); 
            //ciclo while para saber si la linea es distinta de null
            while((l = sr.ReadLine())!=null){
                //ciclo for para recorrer las filas del texto
                for (int i = 0; i < l.Length; i++)
                {
                    //se iguala el char a la posición de la linea en el vector
                    letra = l[i];
                    //si la letra es igual a A entonces
                    if (letra == Convert.ToChar("A"))
                    {
                        //suma los espacios
                        espaciosBlancos++;
                    } //en dado caso la letra es igual a B entonces
                    else if (letra == Convert.ToChar("B"))
                    {
                        //suma las naves
                        spaceBox++;
                    }
                    //en dado caso la letra es igual a C entonces
                    else if (letra == Convert.ToChar("C"))
                    {
                        //suma los asteroides
                        asteroides++;
                    }
                    //en dado caso la letra es igual a D entonces
                    else if (letra == Convert.ToChar("D"))
                    {
                        //suma las tierras
                        tierra++;
                    }
                    //en dado caso la letra es igual a E entonces
                    else if (letra == Convert.ToChar("E"))
                    {
                        //suma los cristales
                        crist++;
                    }
                    //en dado caso la letra no es ninguna de las anteriores entonces
                    else 
                    {
                        //suma los no identificados
                        noIdentificados++;
                    }
                }
                //si el largo de la línea es distinto de 15 entonces
                    if (l.Length != 15)
                    {
                        //cambia el valor a falso
                        largo = false;
                    }
                //suma los caracteres
                cont++;
            }
           //cierra el archivo de lectura
            sr.Close();
            
            // si el contador es distinto de 15 entonces
            if (cont != 15)
            {
                //suma un error
                error += "No tiene 15 líneas" +"\n";
            }
            //si el largo es igual a falso entonces
            if (largo ==false)
            {
                //suma un error
                error += "No tiene solo 15 caracteres" + "\n";
            }
            //si no hay ninguna nave entonces
            if (spaceBox == 0)
            {
                //suma un error
                error += "No tiene ninguna nave" + "\n";
            }
            //si hay mas de una nave entonces
            if (spaceBox > 1)
            {
                //suma un error
                error += "Tiene mas de una nave" + "\n";
            }
            //si hay mas de 7 cristales entonces
            if (crist > 7)
            {
                //suma un error
                error += "Se pasó del límite de cristales disponibles" + "\n";
            }

            //TODO - VALIDAR TIERRA
            
            
            //retorna los errores en dado coso existan 
            return error;
        }
    }
}
