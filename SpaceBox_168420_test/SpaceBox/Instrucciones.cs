﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpaceBox
{
    public partial class Instrucciones : Form
    {
        public Instrucciones()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void Instrucciones_Load(object sender, EventArgs e)
        {

        }

        private void btnMenu1_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
            this.Hide();
        }

        private void btnSalir2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Desea salir?", "SpaceBox", MessageBoxButtons.YesNo))
            {
                this.Close();
            }
        }
    }
}
